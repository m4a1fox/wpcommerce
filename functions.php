<?php
$themename = "Art Nouveau Commerce";
$shortname = "eCommerce_art_nouveau";

$categories = get_categories('hide_empty=0&order_by=name');
$wp_cats = array();

foreach ($categories as $category_list) {
    $wp_cats[$category_list->cat_ID] = $category_list->cat_name;
}

function eCommerce_art_nouveau_content_nav($html_id)
{
    global $wp_query;

    if ($wp_query->max_num_pages > 1) : ?>
        <nav id="<?php echo esc_attr($html_id); ?>">
            <div
                class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'impressionism')); ?></div>
            <div
                class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'impressionism')); ?></div>
        </nav><!-- #nav-above -->
    <?php endif;
}

function eCommerce_art_nouveau_default_admin_banners_title($key, $item = false)
{
    $banners = array(
        'left' => array(
            'title' => 'Top Left Banners'
        ),
        'center_left' => array(
            'title' => 'Top Center Left Banners'
        ),
        'center_center' => array(
            'title' => 'Top Center Center Banners'
        ),
        'center_right' => array(
            'title' => 'Top Center Right Banners'
        ),
        'right' => array(
            'title' => 'Top Right Banners'
        ),
    );

    return $item == false ? $banners[$key] : $banners[$key][$item];
}

function get_category_product_children($catArray, $term_id, $taxonomy, $a)
{
    $cat_child = get_term_children($term_id, $taxonomy);
    if (count($cat_child) > 0) {

        echo '<ul class="children">';
        foreach ($cat_child as $v) {
            if (count(get_term_children($v, $catArray[$v]->taxonomy)) > 0 && $catArray[$v]->category_parent) {
                echo '<li><a href="/?product_cat=' . $catArray[$v]->slug . '">' . $catArray[$v]->name . '</a>';
                get_category_product_children($catArray, $catArray[$v]->term_id, $catArray[$v]->taxonomy, 'b');
            } else {
                if ($catArray[$v]->category_parent == $term_id) {
                    echo '<li><a href="/?product_cat=' . $catArray[$v]->slug . '">' . $catArray[$v]->name . '</a></li>';
                }
            }
        }
        echo '</ul>';
    }
}

function eCommerce_art_nouveau_get_all_category()
{
    $all_category = get_categories('taxonomy=product_cat&hide_empty=0&hierarchical=1');
    $restruct_category_array = array();
    foreach ($all_category as $cat) {
        $restruct_category_array[$cat->cat_ID] = $cat;
    }
    ?>

    <ul>
        <?php
        foreach ($restruct_category_array as $value):?>

            <?php if ($value->category_parent == 0): ?>
                <li class="category-list"><a
                        href="/?product_cat=<?php echo $value->slug; ?>"><?php echo $value->name; ?></a>
                    <?php get_category_product_children($restruct_category_array, $value->term_id, $value->taxonomy, 'nn'); ?>
                </li>
            <?php endif; ?>

        <?php endforeach; ?>
    </ul>
<?php
}


function eCommerce_art_nouveau_add_init()
{
    global $shortname;

    $file_dir = get_bloginfo('template_directory');
    wp_enqueue_style("functions", $file_dir . "/functions/css/functions.css", false, "1.0", "all");
    wp_enqueue_style("$shortname", $file_dir . "/functions/css/jquery-ui.css", false, "1.0", "all");
    wp_enqueue_script("{$shortname}_js", $file_dir . "/functions/js/script.js", false, "1.0");
}

if (get_option($shortname . '_theme_options_menu')) {
    $theme_options_menu = get_option($shortname . '_theme_options_menu');
} else {
    add_option($shortname . '_theme_options_menu', array(
        'top_banners' => array(
            'left' => array(
                'url' => '',
                'link' => '#',
            ),
            'center_left' => array(
                'url' => '',
                'link' => '#',
            ),
            'center_center' => array(
                'url' => '',
                'link' => '#',
            ),
            'center_right' => array(
                'url' => '',
                'link' => '#',
            ),
            'right' => array(
                'url' => '',
                'link' => '#',
            ),
        ),
        'favicon' => '#',
    ));
    $theme_options_menu = get_option($shortname . '_theme_options_menu');
}


function eCommerce_art_nouveau_page_add()
{
    add_submenu_page('themes.php', 'My Theme Options', 'Theme Options', 8, 'themeoptions', 'theme_page_options');
}

function catch_that_image($content)
{

    $first_img = array();

    preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
    $first_img['image'] = $matches [1] [0];
    $first_img['text'] = '<p>' . str_replace($matches [0] [0], '', $content) . '</p>';

    if (empty($first_img['image'])) {
        $first_img['image'] = get_bloginfo('template_directory') . "/img/default.png";
        $first_img['default'] = true;
    }

    return $first_img;
}


function getCategoryPostList($postId)
{


    $categoryId = wp_get_post_categories($postId);
    $categoryList = '';
    foreach ($categoryId as $val) {
        $categoryList .= $val != 1
            ? '<a href="' . get_category_link($val) . '">' . get_the_category_by_ID($val) . '</a>, '
            : '';
    }

    return count($categoryId) == 1 && $categoryId[0] == 1
        ? '&nbsp;'
        : 'Category: ' . rtrim($categoryList, ', ');
}

function eCommerce_art_nouveau_comment($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment;

    $commentId = get_comment_ID();
    $commentClass = get_comment_class($commentId);
    $commentEdit = get_edit_comment_link($commentId);
    $commentAuthor = get_comment_author($commentId);
    $commentText = get_comment_text($commentId);
    $commentDate = get_comment_date('d.m.Y', $commentId);
    $commentReplyLink = get_comment_reply_link(array_merge($args, array('reply_text' => __('Reply'), 'depth' => $depth, 'max_depth' => $args['max_depth'])));
    ?>
    <li <?php echo $commentClass; ?> id="li-comment-<?php echo $commentId; ?>">
    <impressionismicle id="comment-<?php comment_ID(); ?>" class="comment">
        <div class="comment-container">
            <div class="comment comment-header">
                <div class="comment-author">
                    <?php echo $commentAuthor; ?>:
                </div>
                <div class="comment-date">
                    <?php echo $commentDate; ?>
                </div>
            </div>
            <div class="comment comment-content">
                <div class="comment-text">
                    <?php echo $commentText; ?>
                </div>
            </div>
            <div class="comment comment-footer">
                <span class="comment-reply">
                    <?php echo $commentReplyLink; ?>
                </span>
                <span class="comment-edit">
                    <?php if (current_user_can('manage_options')): ?>
                        <a href="<?php echo $commentEdit; ?>">Edit</a>
                    <?php endif; ?>
                </span>
            </div>

        </div>
        <!-- .reply -->
    </impressionismicle>
    <!-- #comment-## -->

<?php
}


add_action('wp_ajax_nopriv_get_post_by_category', 'getPostByCategory');
add_action('wp_ajax_get_post_by_category', 'getPostByCategory');
function getPostByCategory()
{

    $categoryId = htmlspecialchars($_POST['categoryId']);
    $categoryText = htmlspecialchars($_POST['categoryText']);
    $postByCategory = array();
    $args = array('category' => $categoryId);
    $postByCategoryAll = $categoryId != 0 ? get_posts($args) : get_posts();
    foreach ($postByCategoryAll as $key => $val) {
        $postByCategory[$key]['category_id_custom'] = getCategoryPostList($val->ID);
        $postContent = catch_that_image($val->post_content);
        $shortText = explode('<!--more-->', $postContent['text']);
        $postByCategory[$key]['shortText'] = '<p>' . trim($shortText[0] . ' <a href="' . $val->guid . '" class="more-link">more</a></p>');
        $postByCategory[$key]['image'] = $postContent['image'];
        $postByCategory[$key]['id'] = $val->ID;
        $postByCategory[$key]['comment_count'] = $val->comment_count;
        $postByCategory[$key]['guid'] = $val->guid;
        $postByCategory[$key]['post_date'] = $val->post_date;
        $postByCategory[$key]['post_title'] = $val->post_title;
        $postByCategory[$key]['post_type'] = $val->post_type;
        $postByCategory[$key]['post_name'] = $val->post_name;
        $postByCategory[$key]['ping_status'] = $val->ping_status;
    }
    $response = array(
        'catId' => $categoryId,
        'catText' => $categoryText,
        'postByCategory' => $postByCategory,
    );
    $output = json_encode($response);
    print_r($output);
    die();

}


add_action('wp_ajax_save_thumb', 'createThumbnail');
add_action('wp_ajax_nopriv_save_thumb', 'createThumbnail');

function createThumbnail()
{
    $url = $_POST['url'];

    $arrayPath = parse_url($url);
    $parsePath = explode('/', $arrayPath['path']);
    array_pop($parsePath);
    $implodePath = implode('/', $parsePath);
    $dirName = pathinfo($url, PATHINFO_DIRNAME);
    $extension = pathinfo($url, PATHINFO_EXTENSION);
    $fileName = pathinfo($url, PATHINFO_FILENAME);

    $thumb = $implodePath . '/' . $fileName . '_thumb.' . $extension;
    $PathToThumb = $dirName . '/' . $fileName . '_thumb.' . $extension;


    if (!file_exists(ABSPATH . $thumb)) {
        $image = wp_get_image_editor($url);
        if (!is_wp_error($image)) {
            $image->resize(310, 230, true);
            $image->save(ABSPATH . $thumb);
        }
        echo $PathToThumb;
        die();
    } else {
        echo $PathToThumb;
        die();
    }
}

function getAddressLatLng($address)
{
    $address = urlencode(strip_tags($address));

    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
    $raw = file_get_contents($url);
    $data = json_decode($raw);

    $info['lat'] = $data->results[0]->geometry->location->lat;
    $info['lng'] = $data->results[0]->geometry->location->lng;

    return $info;
}


function theme_page_options()
{
    global $themename;
    global $shortname;

    global $theme_options_menu;


    $file_dir = get_bloginfo('template_directory');
    wp_enqueue_script("{$shortname}_js_jquery_ui", $file_dir . "/functions/js/jquery-ui.js", false, "1.0");
    wp_enqueue_script("{$shortname}_js_jquery_scroll_follow", $file_dir . "/functions/js/jquery.scroll-follow.js", false, "1.0");
    wp_enqueue_media();
    wp_enqueue_script('thickbox');
    wp_enqueue_script('media-upload');

    wp_enqueue_style('thickbox');
    wp_enqueue_script("{$shortname}_js_header", $file_dir . "/functions/js/header.js", false, "1.0");
    wp_enqueue_script("{$shortname}_js_gallery", $file_dir . "/functions/js/gallery.js", false, "1.0");


    if ($_POST['form_submit']) {

        $_POST = array_map('stripslashes_deep', $_POST);

        $new_values = array(
            'top_banners' => $_POST['top_banners'],
            'sidebar_banners' => $_POST['sidebar_banners'],
            'site_name' => $_POST['site_name'],
            'site_copyright' => $_POST['site_copyright'],
            'favicon' => $_POST['favicon'],
            'logo' => $_POST['logo'],
        );

        update_option($shortname . '_theme_options_menu', $new_values);
        $theme_options_menu = $new_values;
    }

//    echo '<pre>' . print_r($theme_options_menu, 1) . '</pre>';

    ?>
    <div class="wrap general-settings">
        <table style="width: 100%; position: relative;">
            <tr>
                <td><h2>Theme <?php echo $themename ?> Options</h2></td>
                <td style="width: 270px">
                <span id="impressionism-save-settings" class="round-button">
                    Save Settings
                    <span id="impressionism-save-settings-counter" style="display: none"></span>
                </span>
                    <span class="info"></span>
                    <ul class="docs">
                        <li><a href="<?php echo get_bloginfo('template_directory') ?>/docs/help.html" target="_blank">Documentations</a>
                        </li>
                        <li><a href="<?php echo get_bloginfo('template_directory') ?>/features/features.html"
                               target="_blank">Features</a></li>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="admin-general-block">
            <ul class="admin-menu">
                <li class="js-general"><a href="#" class="selected">General Settings</a></li>
                <li class="js-banners"><a href="#">Top Banners</a></li>
                <li class="sidebar-banners"><a href="#">Sidebar banners</a></li>
            </ul>
            <form action="themes.php?page=themeoptions" method="post" id="impressionism-form-settings">
                <div class="admin-menu-content">
                    <div class="js-general">
                        <h2>General Site Settings</h2>

                        <div class="site-name">
                            <div class="text">Site Name</div>
                            <div class="input">
                                <input type="text" name="site_name"
                                       value="<?php echo $theme_options_menu['site_name']; ?>"/>
                            </div>
                        </div>

                        <div class="site-copyright">
                            <div class="text">Site copyright</div>
                            <div class="input">
                                <input type="text" name="site_copyright"
                                       value="<?php echo $theme_options_menu['site_copyright']; ?>"/>
                            </div>
                        </div>

                        <div class="site-favicon">
                            <div class="text">Site favicon</div>
                            <div id="div_input_favicon" class="input">
                                <div  data-id="favicon">
                                    <a class="button choose-from-library-link" href="#"
                                       data-update-link="favicon">Open Media Library</a>

                                    <div class="select-image-description">Choose your image, then click "Select"
                                        to apply it.
                                    </div>
                                    <input class="imgUrl" id="inp<?php echo $theme_options_menu['favicon']; ?>" type="text"
                                           name="favicon"
                                           value="<?php echo (!empty($theme_options_menu['favicon'])) ? ($theme_options_menu['favicon']) : "Favicon isn't chosen"; ?>"/>
                                </div>

                            </div>
                        </div>
                        <div class="site-logo">
                            <div class="text">Site Logo</div>
                            <div id="div_input_logo" class="input">
                                <div  data-id="logo">
                                    <a class="button choose-from-library-link" href="#"
                                       data-update-link="logo">Open Media Library</a>

                                    <div class="select-image-description">Choose your image, then click "Select"
                                        to apply it.
                                    </div>
                                    <input class="imgUrl" id="inp<?php echo $theme_options_menu['logo']; ?>" type="text"
                                           name="logo"
                                           value="<?php echo (!empty($theme_options_menu['logo'])) ? ($theme_options_menu['logo']) : ""; ?>"/>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="js-banners">
                        <div id="div_inputs_about">
                            <?php foreach ($theme_options_menu['top_banners'] as $key => $val): ?>
                                <h2><?php echo eCommerce_art_nouveau_default_admin_banners_title($key, 'title'); ?></h2>
                                <div data-id="<?php echo $key; ?>" class="js-image-parent-block image-parent-block">
                                    <span class="js-delete-button delete-button"></span>

                                    <div class="media-block">
                                        <div class="gallery-text text">Picture</div>
                                        <div class="gallery-input input">
                                            <a class="button choose-from-library-link" href="#"
                                               data-update-link="<?php echo $key; ?>">Open Media Library</a>

                                            <div class="select-image-description">Choose your image, then click "Select"
                                                to apply it.
                                            </div>
                                            <input class="imgUrl" id="inp<?php echo $key; ?>" type="text"
                                                   name="top_banners[<?php echo $key; ?>][url]"
                                                   value="<?php echo $theme_options_menu['top_banners'][$key]['url']; ?>"/>
                                        </div>
                                        <div class="gallery-text text">Link</div>
                                        <div class="gallery-input input">
                                            <input type="text" name="top_banners[<?php echo $key; ?>][link]"
                                                   value="<?php echo $theme_options_menu['top_banners'][$key]['link']; ?>"/>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="sidebar-banners">
                        <h2>Left&Right Sidebars Images</h2>

                        <div id="inputs_sidebar_banners">
                            <?php foreach ($theme_options_menu['sidebar_banners'] as $key => $val): ?>
                                <div data-id="<?php echo $key; ?>" class="js-image-parent-block image-parent-block">
                                    <span class="js-delete-button delete-button"></span>

                                    <div class="media-block">
                                        <div class="gallery-text text">Picture</div>
                                        <div class="gallery-input input">
                                            <a class="button choose-from-library-link" href="#"
                                               data-update-link="<?php echo $key; ?>">Open Media Library</a>

                                            <div class="select-image-description">Choose your image, then click "Select"
                                                to apply it.
                                            </div>
                                            <input class="imgUrl" id="inp<?php echo $key; ?>" type="text"
                                                   name="sidebar_banners[<?php echo $key; ?>][url]"
                                                   value="<?php echo $val['url']; ?>"/>
                                        </div>
                                    </div>
                                    <div class="link-block">
                                        <div class="link-text text">Link</div>
                                        <div class="link-input input"><input class="link" id="inp<?php echo $key; ?>"
                                                                             type="text"
                                                                             name="sidebar_banners[<?php echo $key; ?>][link]"
                                                                             value="<?php echo $val['link']; ?>"
                                                                             placeholder="http://"/></div>
                                    </div>
                                    <div class="caption-block">
                                        <div class="caption-text text">Caption</div>
                                        <div class="caption-input input">
                                            Left side:&nbsp;&nbsp;<input id="inp<?php echo $key; ?>" type="radio"
                                                                         name="sidebar_banners[<?php echo $key; ?>][position]"
                                                                         value="left"
                                                <?php echo $theme_options_menu['sidebar_banners'][$key]['position'] == 'left' ? 'checked' : '' ?> />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Right side:&nbsp;&nbsp;<input id="inp<?php echo $key; ?>" type="radio"
                                                                          name="sidebar_banners[<?php echo $key; ?>][position]"
                                                                          value="right"
                                                <?php echo $theme_options_menu['sidebar_banners'][$key]['position'] == 'right' ? 'checked' : '' ?>/>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <span id="add-new-block-about" class="round-button">Add New Block</span>
                    </div>
                    <input type="hidden" name="form_submit" value="1"/>
                </div>
            </form>
        </div>
    </div>

<?php
}

function eCommerce_art_nouveau_get_sidebar_template($val, $position)
{
    echo '<ul class="sidebar sidebar_' . $position . '">';
    foreach ($val as $value) {
        echo '<li><a href="' . $value['link'] . '"><img src="' . $value['url'] . '"></a></li>';
    }
    echo '</ul>';
}

function eCommerce_art_nouveau_get_sidebar_tag_cloud($sidebar)
{
    if ($sidebar == 'left') {
        echo '<div id="sidebar">';
        echo '<ul class="ul-cat">';
        dynamic_sidebar('sidebar1');
        echo '</ul>';
        echo '</div>';
    }
}


function eCommerce_art_nouveau_get_sidebar($sidebar = 'left')
{
    global $theme_options_menu;
    $imgArray = array();
    foreach ($theme_options_menu['sidebar_banners'] as $key => $value) {
        if ($value['position'] == $sidebar) {
            $imgArray[$key] = $value;
        }
    }
    if (count($imgArray) > 0) {
        echo '<div class="' . $sidebar . '-sidebar">';
        eCommerce_art_nouveau_get_sidebar_template($imgArray, $sidebar);
        eCommerce_art_nouveau_get_sidebar_tag_cloud($sidebar);
        echo '</div>';
    }
}

add_action('admin_init', 'eCommerce_art_nouveau_add_init');
add_action('admin_menu', 'eCommerce_art_nouveau_page_add');


register_sidebar(array('name' => 'sidebar1'));
register_sidebar(array('name' => 'sidebar2'));


if (!function_exists($shortname . '_single_meta')) {

    /**
     * Output the product meta.
     *
     * @access public
     * @subpackage    Product
     * @return void
     */
    function eCommerce_art_nouveau_single_meta()
    {
        woocommerce_get_template('single-product/meta.php');
    }
}

if (!function_exists($shortname . '_left_sidebar')) {
    function eCommerce_art_nouveau_left_sidebar()
    {
        eCommerce_art_nouveau_get_sidebar();
    }
}

if (!function_exists($shortname . '_right_sidebar')) {
    function eCommerce_art_nouveau_right_sidebar()
    {
        eCommerce_art_nouveau_get_sidebar('right');
    }
}

function get_single_product_image($id = null, $cart = null)
{
    $post_id = $id == null ? get_post_thumbnail_id() : get_post_thumbnail_id($id);
    $image_link = wp_get_attachment_url($post_id);
    $img_width = $cart !== null ? $cart : '105';
    if ($image_link) {
        return '<img src="' . $image_link . '" width="' . $img_width . 'px" />';
    } else {
        return '<img src="' . get_bloginfo('template_directory') . '/img/no-image.png" class="default-product-image" />';
    }
}

function get_single_product_regular_price($id)
{
    $regulat_price = get_post_meta($id, '_regular_price');
    return $regulat_price[0];
}

function get_single_product_by_cat_id($cat_slug)
{
    query_posts('product_cat=' . $cat_slug->slug);
    if (have_posts()) : the_post();
        echo '<div class="product-image">' . get_single_product_image() . '</div>';
        echo '<div class="product-info">';
        echo '<div class="product-title">' . get_the_title() . '</div>';
        echo '<div class="product-price">' . get_current_summ(get_single_product_regular_price(get_the_ID())) . '</div>';
        echo '</div>';


    endif;
    wp_reset_query();
}

if (!function_exists($shortname . '_single_product_from_cat')) {
    function eCommerce_art_nouveau_single_product_from_cat($category)
    {
        get_single_product_by_cat_id($category);
    }
}

function get_current_summ($summ)
{
    return get_woocommerce_currency_symbol() . sprintf("%01.2f", $summ);
}

if (!function_exists($shortname . '_get_cart_product_sidebar')) {
    function eCommerce_art_nouveau_get_cart_product_sidebar()
    {
        global $woocommerce;
        $cart_info = array();
        $cart_product_info = array();
        $cart_info['count_product'] = $woocommerce->cart->cart_contents_count;
        foreach ($woocommerce->cart as $val) {
            foreach ($val as $value) {
                $cart_info['summary'] += get_single_product_regular_price($value['data']->id);
            }
        }

        foreach ($woocommerce->cart->cart_contents as $key => $value) {
            $cart_product_info[$key]['title'] = $value['data']->post->post_title;
            $cart_product_info[$key]['img'] = get_single_product_image($value['data']->id, 70);
            $cart_product_info[$key]['quantity'] = $value['quantity'];
            $cart_product_info[$key]['summary'] = get_single_product_regular_price($value['data']->id);
        }


        echo '<div class="sidebar_cart user_cart">';
        echo '<div class="cart_info"><h3><a href="' . $woocommerce->cart->get_cart_url() . '">My cart</a></h3>';
        echo '<span class="count_product">' . $cart_info['count_product'] . ' items</span>';
        echo '</div>';
        echo '<div class="summary">' . $woocommerce->cart->get_cart_total() . '</div>';
        if (count($cart_product_info) > 0) {
            $i = 0;
            foreach ($cart_product_info as $key => $info) {
                if ($i <= 2) {
                    echo '<div class="product_info">';
                    echo '<h3>' . $info['title'] . '</h3>';
                    echo '<div class="product_img">';
                    echo $info['img'];
                    echo '<div class="product_summ">';
                    echo '<div class="product_cost">';
                    echo get_current_summ($info['summary']);
                    echo '</div>';
                    echo '<div class="product_quantity">';
                    echo '*' . $info['quantity'];
                    echo '<span class="remove_item"><a href="' . esc_url($woocommerce->cart->get_remove_url($key)) . '"></a></span>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '<div class="product_price">';
                    echo $info['price'];
                    echo '</div>';
                    echo '</div>';
                }
                $i++;

            }
        } else {
            echo '<div class="product_info">No items in cart</div>';
        }


        echo '</div>';
    }
}

add_action($shortname . '_single_product_from_cat_display', $shortname . '_single_product_from_cat');
add_action($shortname . '_right_sidebar_display', $shortname . '_right_sidebar');
add_action($shortname . '_get_cart_product_sidebar_display', $shortname . '_get_cart_product_sidebar');
add_action($shortname . '_left_sidebar_display', $shortname . '_left_sidebar');


/**
 * Product Summary Box
 *
 * @see woocommerce_template_single_title()
 * @see woocommerce_template_single_price()
 * @see woocommerce_template_single_excerpt()
 * @see woocommerce_template_single_meta()
 * @see woocommerce_template_single_sharing()
 */

add_action($shortname . '_product_category', $shortname . '_single_meta', 40);


function getPriceHTML($specialPrice = 0, $regularPrice)
{
    $html = "<p>Regular price: " . woocommerce_price($regularPrice) . "</p>";

    $html .= ($specialPrice && $specialPrice != $regularPrice) ? "<p class='special-price'>Special price: " . woocommerce_price($specialPrice) . "</p>" : "";

    return $html;
}

function impressionism_single_product_small_thumbnail_size($size)
{
    $size = 'full';
    return $size;
}

// Filter wp_nav_menu() to add additional links and other output
function new_nav_menu_items($items)
{
    $page = get_page(17);
    $items = str_replace('<li class="page_item page-item-' . $page->ID . '"><a href="' . $page->guid . '">' . $page->post_title . '</a></li>', '', $items);
    if (!is_user_logged_in()) {
        $pageFrom = get_page(21);
        $pageTo = get_page(19);
        $page20 = get_page(20);
        $page18 = get_page(18);
        $items = str_replace('<li class="page_item page-item-' . $page20->ID . '"><a href="' . $page20->guid . '">' . $page20->post_title . '</a></li>', '', $items);
        $items = str_replace('<li class="page_item page-item-' . $page18->ID . '"><a href="' . $page18->guid . '">' . $page18->post_title . '</a></li>', '', $items);
        $items = str_replace(
            '<li class="page_item page-item-' . $pageFrom->ID . '"><a href="' . $pageFrom->guid . '">' . $pageFrom->post_title . '</a></li>',
            '<li class="page_item page-item-' . $pageTo->ID . '"><a href="' . $pageTo->guid . '">Login</a></li>',
            $items);
    }
    return $items;
}

add_filter('wp_list_pages', 'new_nav_menu_items');
