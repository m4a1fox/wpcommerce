<?php $options = get_option('eCommerce_art_nouveau_theme_options_menu');?>
</div>
<div class="footer">
    <div class="footer_menu">
        <?php wp_nav_menu(array('depth' => 1)); ?>
    </div>
    <div class="copyright">
        <?php echo $options['site_copyright']?>
    </div>
</div>
<div id="dialog-message">
    <p>
        <span id="add-to-cart" class="ui-icon ui-icon-circle-check"></span>
    </p>
</div>
</body>
</html>