<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product, $woocommerce;

?>
<div class="images single-product-photos">
	<ul id="pikame" >
		<?php
			if ( has_post_thumbnail() ) {

	//			$image       		= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
				$image       		= get_the_post_thumbnail( $post->ID, 'full' );
				$image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
				$image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
				$attachment_count   = count( get_children( array( 'post_parent' => $post->ID, 'post_mime_type' => 'image', 'post_type' => 'attachment' ) ) );
				list($real_width, $real_height, $type, $attr) = getimagesize($image_link);

				if ( $attachment_count != 1 ) {
					$gallery = '[product-gallery]';
				} else {
					$gallery = '';
				}

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a></li>', $image_link, $image_title, $image ), $post->ID );
	//			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<div id="zoom01" class="zoom">%s</div>', $image ), $post->ID );

			} else {

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><img src="%s" alt="Placeholder" /></li>', woocommerce_placeholder_img_src() ), $post->ID );

			}

			$attachment_ids = $product->get_gallery_attachment_ids();

			if ( $attachment_ids ) {

				$loop = 0;
				$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );

				foreach ( $attachment_ids as $attachment_id ) {

					$classes = array( 'zoom' );

					if ( $loop == 0 || $loop % $columns == 0 )
						$classes[] = 'first';

					if ( ( $loop + 1 ) % $columns == 0 )
						$classes[] = 'last';

					$image_link = wp_get_attachment_url( $attachment_id );

					if ( ! $image_link )
						continue;

					$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'impressionism_single_product_small_thumbnail_size' ) );
					$image_class = esc_attr( implode( ' ', $classes ) );
					$image_title = esc_attr( get_the_title( $attachment_id ) );

					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li><a href="%s" class="%s product-thumbs" title="%s"  rel="prettyPhoto[product-gallery]">%s</a></li>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );

					$loop++;
				}
			}
			// do_action( 'woocommerce_product_thumbnails' );
		?>
	</ul>
</div>

<?php

	$wto = 372;
	$hto = ($real_height * $wto) / $real_width;

	echo '<script type= "text/javascript">
			/*<![CDATA[*/
			var u = 0;
			$("#pikame").PikaChoose({
				autoPlay:true,
				showCaption:false,
				text: { previous: "", next: "" },
				IESafe:true,
				thumbOpacity:1,
				carousel:true,
				autoPlay:false,
				animationFinished: function(x) {
					if (u) {
						$( "#sliderProductImg" ).slider( { value: 0 } );
						var img = new Image();
						img.src = $(".pika-stage > a > img").attr("src");
						$(".pika-stage > a > img").height(350);
						$(".pika-stage > a > img").width((350*img.width)/img.height);
					}
					u++;
				}
			});
			/*]]>*/
		</script>';
