<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
?>
	</div>
<?php
    echo '<div class="sidebar right">';
    do_action('eCommerce_art_nouveau_get_cart_product_sidebar_display');
    do_action('eCommerce_art_nouveau_right_sidebar_display');
?>
</div>
