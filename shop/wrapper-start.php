<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$template = get_option('template');

echo '<div id="container">';

if(is_front_page() || is_product_category()){
    do_action('eCommerce_art_nouveau_left_sidebar_display');
}

echo '<div id="content" role="main" class="">';

