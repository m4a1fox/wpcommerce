<?php $options = get_option('eCommerce_art_nouveau_theme_options_menu');?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset');?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo wp_title('<<', true, 'right');?> | <?php echo bloginfo('name');?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <?php if(!empty($options['favicon'])) : ?>
        <link rel="shortcut icon" href="<?php echo $options['favicon']; ?>" />
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/custom-dropdown.css';?>" type="text/css" media="screen" />
    <!--[if lt IE 9]>
        <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/support-html5-oldie.js';?>'></script>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/' . 'ie.css';?>" />
    <![endif]-->
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/functions/js/jquery-1.9.1.js';?>'></script>
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery.custom-dropdown.js';?>'></script>

    <!-- pop-up notice when product was added to cart-->
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/add-to-cart-pop-up.js';?>'></script>

    <!-- Single Product Images -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/jquery-ui-1.10.3.custom.css';?>" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/jquery.gzoom.css';?>" type="text/css" media="screen" />
		<script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery-ui.js';?>'></script>
		<script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery.mousewheel.js';?>'></script>
		<script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery.gzoom.js';?>'></script>

		<link type="text/css" href="<?php echo get_template_directory_uri() . '/libs/pikachoose/styles/bottom.css';?>" rel="stylesheet" />
		<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/libs/pikachoose/lib/jquery.jcarousel.min.js';?>"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/libs/pikachoose/lib/jquery.pikachoose.art.js';?>"></script>
	<!-- End Single Product IMages -->


    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/script.js';?>'></script>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="header-bg">
            <div class="category">
                <?php echo eCommerce_art_nouveau_get_all_category();?>
            </div>
            <div class="logo-user-menu">
                <div class="overflow-hidden" style="width: 960px;">
                    <a href="/" class="no-decoration">
                        <div class="header-logo text-align-center">
                            <?php if(!empty($options['logo'])):?>
                            <img src="<?php echo $options['logo']; ?>" alt=""/>
                            <?php endif;?>
                            <h1 class="slogan font-swaak"><?php echo strtoupper($options['site_name']);?></h1>
                        </div>
                    </a>
                </div>
                <div class="user-menu">
                    <?php echo wp_nav_menu(array('depth' => 0, 'container' => 'div'));?>
                </div>
                <div class="search-form">
                    <?php echo get_search_form( $echo );?>
                </div>
            </div>

        </div>
    </div>

    <div class="content-block greyBlock border-tb">
        <div class="fixed-width">
            <div class="greyBlockHead z-index-1 position-absolute"></div>
<?php if(is_front_page()):?>
            <div class="greyBlockContent z-index-2 position-relative">
                <div class="banner-left">
                    <img src="<?php echo $options['top_banners']['left']['url'] ?>" alt=""/>
                </div>
                <div class="banner-center">
                    <div class="banner-center-left">
                        <img src="<?php echo $options['top_banners']['center_left']['url'] ?>" alt=""/>
                    </div>
                    <div class="banner-center-cetner">
                        <img src="<?php echo $options['top_banners']['center_center']['url'] ?>" alt=""/>
                    </div>
                    <div class="banner-center-right">
                        <img src="<?php echo $options['top_banners']['center_right']['url'] ?>" alt=""/>
                    </div>
                </div>
                <div class="banner-right">
                    <img src="<?php echo $options['top_banners']['right']['url'] ?>" alt=""/>
                </div>
            </div>
<?php else:?>

<?php endif;?>
        </div>
    </div>
