jQuery(document).ready(function(){

    jQuery("ul.admin-menu li").click(function(e){
        e.preventDefault();
        jQuery("ul.admin-menu li a.selected").removeClass('selected');
        jQuery("a", this).addClass('selected');
        var block = jQuery(this).attr('class');
        jQuery(".admin-menu-content > div").hide();
        jQuery(".admin-menu-content > div."+ block).show();
    });

    jQuery('.info').click(function(){
        jQuery('.docs').show();
    });

    jQuery('body').click(function(e){
        if(e.target.className != 'info'){
            jQuery('.docs').hide();
        }
    });
});