/**
 * Gallery. Start Code.
 */
var frame;
(function ($) {
    function getImageUrl(event, buttonLink, parentBlock, createThumb){
        createThumb = !!(createThumb !== undefined);

        var $el = $(buttonLink);
        parentBlock.data('lastClicked', $el.data('updateLink'));
        event.preventDefault();

        if (frame) {
            frame.open();
            return;
        }

        frame = wp.media.frames.customHeader = wp.media({
            title: $el.data('choose'),
            library: {
                type: 'image'
            },
            button: {
                text: $el.data('update'),
                close: false
            }
        });

        frame.on('select', function () {

            var attachment = frame.state().get('selection').first(),
                imgUrl = attachment.attributes.url,
                inputId = parentBlock.data('lastClicked'),
                inputField = $("div[data-id='"+inputId+"']", parentBlock);
            inputField.find('input.imgUrl').val(imgUrl);
            inputField.find('input.imgUrl').show();

            if(createThumb){
                $('#impressionism-save-settings').addClass('disabled');
                inputField.find('.js-create-thumb-process-image').css({'display': 'inline-block'});
                var data = {action: 'save_thumb', url: imgUrl};
                $.ajax({
                    type: "POST",
                    url: "/wp-admin/admin-ajax.php",
                    data: data,
                    success: function(data){
                        inputField.find('input.imgUrlThumb').val(data);
                        inputField.find('.js-create-thumb-process-image').removeAttr('style');
                        $('#impressionism-save-settings').removeClass('disabled');
                    }
                });
            }

            frame.close();
        });
        frame.open();
    }

    $(function () {
        $('#div_inputs_about').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('#div_inputs_about'));
        });

        $('#inputs_sidebar_banners').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('#inputs_sidebar_banners'));
        });

        $('#div_input_favicon').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('#div_input_favicon'));
        });

        $('#div_input_logo').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('#div_input_logo'));
        });



    });
}(jQuery));

/**
 * Gallery End Code.
 */