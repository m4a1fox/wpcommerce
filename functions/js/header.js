jQuery(document).ready(function () {
    function countImagesBlock(block) {
        var col = jQuery("> div:last", block).data("id");
        if (col == undefined) {col = 0;}

        return ++col;
    }

    function imageBlock(col_inputs, whichBlock) {
        var blocks = [];

        blocks['sidebarBanners'] = '' +
            '<div data-id="' + col_inputs + '" class="js-image-parent-block image-parent-block">' +
            '<span class="js-delete-button delete-button"></span>' +
            '<div class="media-block">' +
            '<div class="gallery-text text">Picture</div>' +
            '<div class="gallery-input input">' +
            '<a class="button choose-from-library-link"  href="#" data-update-link="' + col_inputs + '">Open Media Library</a>' +
            '<div class="select-image-description">Choose your image, then click "Select" to apply it.</div>' +
            '<input class="imgUrl" style="display: none;" id="inp' + col_inputs + '" type="text" name="sidebar_banners[' + col_inputs + '][url]" value="" />' +
            '</div>' +
            '</div>' +
            '<div class="link-block">' +
            '<div class="link-text text">Link</div>' +
            '<div class="link-input input"><input class="link" id="inp' + col_inputs + '" type="text" name="sidebar_banners[' + col_inputs + '][link]" value="" placeholder="http://" /></div>' +
            '</div>' +
            '<div class="caption-block">' +
            '<div class="caption-text text">Caption</div>' +
            '<div class="caption-input input">' +
            'Left side:&nbsp;&nbsp;<input id="inp' + col_inputs + '" type="radio" name="sidebar_banners[' + col_inputs + '][position]" value="left" checked />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            'Right side:&nbsp;&nbsp;<input id="inp' + col_inputs + '" type="radio" name="sidebar_banners[' + col_inputs + '][position]" value="right" />' +
            '</div>' +
            '</div>' +
            '</div>'
        ;

        return blocks[whichBlock];
    }
    var i = 0;
    function saveSettingCounter() {
        var counterSpan = jQuery('#impressionism-save-settings-counter');
        ++i;
        if (i > 0) {

            counterSpan.show();
            counterSpan.text(i);
        }
    }

    jQuery("form :input").change(function () {
        saveSettingCounter();
    });


    jQuery('.general-settings #impressionism-save-settings').click(function () {
        if(!jQuery(this).hasClass('disabled')){
            jQuery('.general-settings #impressionism-form-settings').submit();
        }
    });

    jQuery("#div_inputs").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });

    jQuery("#add-new-block-about").on('click', function () {
        var block = imageBlock(countImagesBlock(jQuery(".sidebar-banners #inputs_sidebar_banners")), 'sidebarBanners');
        jQuery(block).appendTo(jQuery("#inputs_sidebar_banners"));
    });

    jQuery("#inputs_sidebar_banners").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });

});