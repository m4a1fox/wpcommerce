<?php $option = get_option('eCommerce_art_nouveau_theme_options_menu');?>
<div class="content-block greyBlock border-tb">
    <div class="fixed-width">
        <div class="greyBlockHead z-index-1 position-absolute"></div>
        <div class="greyBlockContent z-index-2 position-relative">
            <div class="banner-left">
                <img src="<?php echo $option['top_banners']['left']['url'] ?>" alt=""/>
            </div>
            <div class="banner-center">
                <div class="banner-center-left">
                    <img src="<?php echo $option['top_banners']['center_left']['url'] ?>" alt=""/>
                </div>
                <div class="banner-center-cetner">
                    <img src="<?php echo $option['top_banners']['center_center']['url'] ?>" alt=""/>
                </div>
                <div class="banner-center-right">
                    <img src="<?php echo $option['top_banners']['center_right']['url'] ?>" alt=""/>
                </div>
            </div>
            <div class="banner-right">
                <img src="<?php echo $option['top_banners']['right']['url'] ?>" alt=""/>
            </div>
        </div>
    </div>
</div>