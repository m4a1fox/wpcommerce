<?php get_header(); ?>

<div class="header">
    <div class="header-bg">
        <div class="fixed-width page-content">
            <h2 class="font-swaak active_color"><?php echo strtoupper(get_the_title()); ?></h2>
            <?php if (have_posts()) { ?>
            <?php while (have_posts()) {
                the_post();
            } ?>
            <div class="post">
                <?php the_content() ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>