$(document).ready(function () {
    $('a.add_to_cart_button, .single_add_to_cart_button').click(function(evn){
        evn.preventDefault();
        //check for single or category(related) adding to cart then form href and product name
        if($(this).parent().prop("tagName") == "FORM"){
            $href = $(this).parent().attr("action");
            $product_name = $(this).parent().parent().find('h2').text();
        }else{
            $href = $(this).attr("href");
            $product_name = $(this).parent().find('h3').text();
        }
        $paragraph = $("#dialog-message p");
        //form pop-up message
        $paragraph.text($product_name + " has been added to cart");
        $(function() {
            $( "#dialog-message" ).dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        $( this ).dialog( "close" );
                        $paragraph.text('');
                        window.location.href = $href;
                    }
                }
            });
            $(".ui-dialog-titlebar").hide();
        });
    });
    //hide default woocommerce message
    $("div.woocommerce-message:contains('successfully added to your cart')").hide();
});


