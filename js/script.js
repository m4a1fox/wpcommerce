$(document).ready(function () {
    $('a.button', '.add_review').click(function(e){
        e.preventDefault();
        $('#review_form_wrapper').slideToggle();
    });

    $('a', '.noreviews').click(function(e){
        e.preventDefault();
        $('#review_form_wrapper').slideToggle();
    });

    var commentForm = $('#commentform'),
        commentsInputsTextArea = $('input[type="text"], textarea', commentForm),
        placeholderText = {
            author: $('label', '.comment-form-author').text()+'*',
            email: $('label', '.comment-form-email').text()+'*',
            comment: $('label', '.comment-form-comment').text()+'*'
        },
        commentsTextAlignLeft = {'text-align': 'right'};

    commentsInputsTextArea.each(function () {
        var inputId = $(this).attr('id');
        $(this).attr('placeholder', placeholderText[inputId]);
        $(this).focus(function () {
            if ($(this).val().length == 0) {
                $(this).css(commentsTextAlignLeft).attr('placeholder', '');
            }
        });

        $(this).blur(function () {
            if ($(this).val().length == 0) {
                $(this).attr('placeholder', placeholderText[inputId]).removeAttr('style');
            }
        });
    });

    var starRating = function(){
        var html = '<ul id="rating">';
        html += '<li data-value="5"></li>';
        html += '<li data-value="4"></li>';
        html += '<li data-value="3"></li>';
        html += '<li data-value="2"></li>';
        html += '<li data-value="1"></li>';
        html += '</ul>';
        return html;
    }



    var commentRatingSelect = $('.comment-form-rating');
    $('select', commentRatingSelect).hide();
    commentRatingSelect.prepend(starRating);

    $('ul li', commentRatingSelect).on('click', function(){
        var $_this = $(this);
        $_this.siblings().removeClass('active');
        $_this.addClass('active');
        var $_value = $_this.data('value');
        $('select option[value="'+ $_value +'"]', commentRatingSelect).prop('selected', true);
    });
});




$(document).ready(function (){
	var HH = 0;
	var WW = 0;

	$( "#sliderProductImg" ).slider({
		step: 1,
		min: 0,
		max: 100,
		change: function( event, ui) {
			slider( ui.value );
		},
		slide: function( event, ui) {
			slider( ui.value );
		},
		start: function( event, ui ) {
			startSize();
		}
	});

	$('.mmminus').click(function(){
		startSize();
		$( "#sliderProductImg" ).slider( "option", "value", $( "#sliderProductImg" ).slider( "option", "value") - 1 );
	});

	$('.mmplus').click(function(){
		startSize();
		$( "#sliderProductImg" ).slider( "option", "value", $( "#sliderProductImg" ).slider( "option", "value") + 1 );
	});

	function startSize() {
		if (!HH && !WW) {
			var image = $(".pika-stage > a > img");
			HH = $(image).height();
			WW = $(image).width();
		}
	}
	function slider(value) {
		var image = $(".pika-stage > a > img");
		var realSize = naturalSize($(image).attr('src'));

		var pH = HH + ((value * HH) / realSize.h);
		var pW = WW + ((value * WW) / realSize.w);

		$(image).height(pH);
		$(image).width(pW);

	}

	function naturalSize(src) {
		var img = new Image();
		img.src = src;

		return {h: img.height, w: img.width};
	}


});

